let startBtn = document.getElementById('start-btn');
let restartBtn = document.getElementById('restart-btn');
let playingField = document.getElementById('game');
let startWindow = document.getElementById('start');
let scoreCount = document.getElementById('#score');
let screenW = document.getElementById('app').clientWidth;
let screenH = document.getElementById('app').clientHeight;
let soundPlay = document.querySelector('audio');
let soundBtn = document.querySelector('#sound img');
let soundSource = document.querySelector('audio source');
let soundStatus = 'off';
let character = document.getElementById('player');
let countLifes = 1;
let score = document.querySelector('#score span');
let speed = 80;
let characterSkin = 'skin_1'
let selectSkinOne = document.getElementById('skin_1');
let selectSkinTwo = document.getElementById('skin_2');

const startGame = () => {
	startCombat();
}

selectSkinOne.addEventListener("click", () => {
	selectSkinOne.classList.add('selected');
	selectSkinTwo.classList.remove('selected');
	characterSkin = 'skin_1';
});

selectSkinTwo.addEventListener("click", () => {
	selectSkinTwo.classList.add('selected');
	selectSkinOne.classList.remove('selected');
	characterSkin = 'skin_2'
});

startBtn.addEventListener("click", startGame);

const startCombat = () => {
	startWindow.style.display = 'none';
	playingField.style.display = 'block';
	character.className = characterSkin;
	createLifes();
	createEnemy();
	// setTimeout(createEnemy, 1000);
	// createBullet();
}



const typeEnemy = () => {
	if (random(1, 2) == 1) {
		return 'type-1';
	} else {
		return 'type-2';
	}
}

const createEnemy = () => {
	let enemy = document.createElement('div');
	enemy.className = 'enemy ' + typeEnemy();
	enemy.style.top = random(120, screenH - 150) + 'px';
	playingField.appendChild(enemy);
	moveEnemy(enemy);
}

const speedUp = () => {
	switch (score.innerText) {
		case '25':
			speed = 60;
			break;
		case '50':
			speed = 50;
			break;
		case '75':
			speed = 30;
			break;
	}
}

const moveEnemy = (enemy) => {
	let timerID = setInterval(() => {
		enemy.style.left = enemy.offsetLeft - 10 + 'px';
		if (enemy.offsetLeft < -100) {
			enemy.remove();
			createEnemy();
			clearInterval(timerID);
			characterDie();
		}
		speedUp();
	}, speed)
}

const createBullet = () => {
	let bullet = document.createElement('div');
	bullet.className = 'bullet';
	bullet.style.top = character.offsetTop + 140 + 'px';
	playingField.appendChild(bullet);
	moveBullet(bullet);
}

const moveBullet = (bullet) => {
	let timerID = setInterval(() => {
		bullet.style.left = bullet.offsetLeft + 10 + 'px';
		if (bullet.offsetLeft > screenW) {
			bullet.remove();
			// createBullet();
			clearInterval(timerID);
		}
		isBoom(bullet);
	}, 10)
}

const isBoom = (bullet) => {
	let enemy = document.querySelector('.enemy');
	if (bullet.offsetTop > enemy.offsetTop
		&& bullet.offsetTop < enemy.offsetTop + enemy.clientHeight
		&& bullet.offsetLeft > enemy.offsetLeft) {
		createBoom(bullet.offsetTop, bullet.offsetLeft)
		score.innerText = Number(score.innerText) + 1;
		bullet.remove();
		enemy.remove();
		createEnemy();
	}
}

const createBoom = (top, left) => {
	let boom = document.createElement('div');
	boom.className = 'boom';
	boom.style.top = top - 100 + 'px';
	boom.style.left = left - 100 + 'px';
	playingField.appendChild(boom);
	setTimeout(() => { boom.remove(); }, 500);
}

const characterDie = () => {
	countLifes = countLifes - 1;
	if (countLifes <= 0) {
		endGame();
	}
	createLifes();
}

const createLifes = () => {
	let lifesCount = document.getElementById('lifes');
	lifesCount.innerHTML = '';
	let count = 0;
	while (count < countLifes) {
		let span = document.createElement('span');
		lifesCount.appendChild(span);
		count++
	}
}

const endGame = () => {
	let endScore = document.querySelector('#end h3 span');
	endScore.innerText = score.innerText;
	playingField.innerHTML = '';
	let endWindow = document.getElementById('end');
	endWindow.style.display = 'block';
}

const restart = () => {
	location.reload();
}

restartBtn.addEventListener("click", restart);

const random = (min, max) => {
	// random number from min to (max+1)
	let rand = min + Math.random() * (max + 1 - min);
	return Math.floor(rand);
}

const soundToggle = () => {
	if (soundStatus == 'on') {
		soundBtn.src = 'images/mute_sound.png';
		soundPlay.pause();
		soundStatus = 'off'
	} else {
		soundBtn.src = 'images/sound_on.png';
		soundPlay.volume = 0.1;
		soundPlay.play();
		soundStatus = 'on'
	}
}

soundBtn.addEventListener("click", soundToggle);

document.onkeydown = (event) => {
	if (event.keyCode == 40 && character.offsetTop < screenH - 250) {
		character.style.top = character.offsetTop + 40 + 'px';
	} else if (event.keyCode == 38 && character.offsetTop > 60) {
		character.style.top = character.offsetTop - 40 + 'px';
	} else if (event.keyCode == 32) {
		createBullet();
	}
}